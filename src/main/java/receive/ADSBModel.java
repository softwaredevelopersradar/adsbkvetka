package receive;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class ADSBModel {

    private Byte ID;
    private SimpleStringProperty MessageType = new SimpleStringProperty();
    private Byte TransmissionType;
    private SimpleStringProperty SessionId = new SimpleStringProperty();
    private SimpleStringProperty AircraftId = new SimpleStringProperty();
    private SimpleStringProperty ICAO = new SimpleStringProperty();
    private SimpleStringProperty FlightId = new SimpleStringProperty();
    private Date GeneratedDate = new Date();
    private Date GeneratedTime = new Date();
    private Date LoggedDate = new Date();
    private Date LoggedTime = new Date();
    private Date CurrentDateTime = new Date();

    private SimpleStringProperty Callsign = new SimpleStringProperty();
    private SimpleIntegerProperty Altitude = new SimpleIntegerProperty();
    private SimpleIntegerProperty GroundSpeed = new SimpleIntegerProperty();
    private SimpleIntegerProperty TrackAngle = new SimpleIntegerProperty();
    private SimpleDoubleProperty Latitude = new SimpleDoubleProperty();
    private SimpleDoubleProperty Longitude = new SimpleDoubleProperty();
    private SimpleDoubleProperty VerticalRate = new SimpleDoubleProperty();
    private SimpleIntegerProperty Squawk = new SimpleIntegerProperty();
    private SimpleDoubleProperty Distance = new SimpleDoubleProperty();
    private Byte Alert;
    private Byte Emergency;
    private Byte SPI;
    private Byte IsOnEarth;
    private SimpleStringProperty LastSignal = new SimpleStringProperty();


    public ADSBModel() {
    }

//    public ADS_B_Model(ADS_B_Model ads_b_model) {
//        this.ID = ads_b_model.ID;
//        this.message_type = ads_b_model.message_type;
//        this.transmission_type = ads_b_model.transmission_type;
//        this.session_id = ads_b_model.session_id;
//        this.aircraft_id = ads_b_model.aircraft_id;
//        this.hex_ident = ads_b_model.hex_ident;
//        this.flight_id = ads_b_model.flight_id;
//        this.generated_date = ads_b_model.generated_date;
//        this.generated_time = ads_b_model.generated_time;
//        this.logged_date = ads_b_model.logged_date;
//        this.logged_time = ads_b_model.logged_time;
//        this.callsign = ads_b_model.callsign;
//        this.altitude = ads_b_model.altitude;
//        this.ground_speed = ads_b_model.ground_speed;
//        this.track = ads_b_model.track;
//        this.lat = ads_b_model.lat;
//        this.lon = ads_b_model.lon;
//        this.vertical_rate = ads_b_model.vertical_rate;
//        this.squawk = ads_b_model.squawk;
//        this.alert = ads_b_model.alert;
//        this.emergency = ads_b_model.emergency;
//        this.spi = ads_b_model.spi;
//        this.is_on_ground = ads_b_model.is_on_ground;
//        this.lastSignal = ads_b_model.lastSignal;
//        this.distance = ads_b_model.distance;
//    }
//

    public ADSBModel(Byte ID, String hex_ident, String callsign, Double lat, Double lon, Integer altitude, Integer ground_speed, Double distance,
                     Integer track, Double vertical_rate, Integer squawk, Byte alert, Byte emergency, Byte spi,
                     Byte is_on_ground) {
        this.ID = ID;
        this.ICAO = new SimpleStringProperty(hex_ident);
        this.Callsign = new SimpleStringProperty(callsign);
        this.Latitude = new SimpleDoubleProperty(lat);
        this.Longitude = new SimpleDoubleProperty(lon);
        this.Altitude = new SimpleIntegerProperty(altitude);
        this.GroundSpeed = new SimpleIntegerProperty(ground_speed);
        this.Distance = new SimpleDoubleProperty(distance);
        this.TrackAngle = new SimpleIntegerProperty(track);
        this.VerticalRate = new SimpleDoubleProperty(vertical_rate);
        this.Squawk = new SimpleIntegerProperty(squawk);
        this.Alert = alert;
        this.Emergency = emergency;
        this.SPI = spi;
        this.IsOnEarth = is_on_ground;
    }

    public ADSBModel(Byte ID, String message_type, Byte transmission_type, String session_id, String aircraft_id, String hex_ident,
                     String flight_id, Date generated_date, Date generated_time, Date logged_date, Date logged_time,
                     Date currentDateTime,
                     String callsign, Integer altitude, Integer ground_speed, Integer track, Double lat, Double lon,
                     Double vertical_rate, Integer squawk, Byte alert, Byte emergency, Byte spi, Byte is_on_ground,
                     String lastSignal, Double distance) {
        this.ID = ID;
        this.MessageType = new SimpleStringProperty(message_type);
        this.TransmissionType = transmission_type;
        this.SessionId = new SimpleStringProperty(session_id);
        this.AircraftId = new SimpleStringProperty(aircraft_id);
        this.ICAO = new SimpleStringProperty(hex_ident);
        this.FlightId = new SimpleStringProperty(flight_id);
        this.GeneratedDate = new Date(generated_date.getTime());
        this.GeneratedTime = new Date(generated_time.getTime());
        this.LoggedDate = new Date(logged_date.getTime());
        this.LoggedTime = new Date(logged_time.getTime());
        this.CurrentDateTime = new Date(currentDateTime.getTime());
        this.Callsign = new SimpleStringProperty(callsign);
        this.Latitude = new SimpleDoubleProperty(lat);
        this.Longitude = new SimpleDoubleProperty(lon);
        this.Altitude = new SimpleIntegerProperty(altitude);
        this.GroundSpeed = new SimpleIntegerProperty(ground_speed);
        this.TrackAngle = new SimpleIntegerProperty(track);
        this.VerticalRate = new SimpleDoubleProperty(vertical_rate);
        this.Squawk = new SimpleIntegerProperty(squawk);
        this.Distance = new SimpleDoubleProperty(distance);
        this.Alert = alert;
        this.Emergency = emergency;
        this.SPI = spi;
        this.IsOnEarth = is_on_ground;
        this.LastSignal = new SimpleStringProperty(lastSignal);
    }

    public Byte getID() {
        return ID;
    }

    public void setID(Byte ID) {
        this.ID = ID;
    }

    public String getMessageType() {
        return MessageType.get();
    }

    public SimpleStringProperty messageTypeProperty() {
        return MessageType;
    }

    public void setMessageType(String messageType) {
        this.MessageType.set(messageType);
    }

    public Byte getTransmissionType() {
        return TransmissionType;
    }

    public void setTransmissionType(Byte transmissionType) {
        TransmissionType = transmissionType;
    }

    public String getSessionId() {
        return SessionId.get();
    }

    public SimpleStringProperty sessionIdProperty() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        this.SessionId.set(sessionId);
    }

    public String getAircraftId() {
        return AircraftId.get();
    }

    public SimpleStringProperty aircraftIdProperty() {
        return AircraftId;
    }

    public void setAircraftId(String aircraftId) {
        this.AircraftId.set(aircraftId);
    }

    public String getICAO() {
        return ICAO.get();
    }

    public SimpleStringProperty ICAOProperty() {
        return ICAO;
    }

    public void setICAO(String ICAO) {
        this.ICAO.set(ICAO);
    }

    public String getFlightId() {
        return FlightId.get();
    }

    public SimpleStringProperty flightIdProperty() {
        return FlightId;
    }

    public void setFlightId(String flightId) {
        this.FlightId.set(flightId);
    }

    public Date getGeneratedDate() {
        return GeneratedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        GeneratedDate = generatedDate;
    }

    public Date getGeneratedTime() {
        return GeneratedTime;
    }

    public void setGeneratedTime(Date generatedTime) {
        GeneratedTime = generatedTime;
    }

    public Date getLoggedDate() {
        return LoggedDate;
    }

    public void setLoggedDate(Date loggedDate) {
        LoggedDate = loggedDate;
    }

    public Date getLoggedTime() {
        return LoggedTime;
    }

    public void setLoggedTime(Date loggedTime) {
        LoggedTime = loggedTime;
    }

    public String getCallsign() {
        return Callsign.get();
    }

    public SimpleStringProperty callsignProperty() {
        return Callsign;
    }

    public void setCallsign(String callsign) {
        this.Callsign.set(callsign);
    }

    public int getAltitude() {
        return Altitude.get();
    }

    public SimpleIntegerProperty altitudeProperty() {
        return Altitude;
    }

    public void setAltitude(Integer altitude) {
        this.Altitude.set(altitude);
    }

    public int getGroundSpeed() {
        return GroundSpeed.get();
    }

    public SimpleIntegerProperty groundSpeedProperty() {
        return GroundSpeed;
    }

    public void setGroundSpeed(Integer groundSpeed) {
        this.GroundSpeed.set(groundSpeed);
    }

    public int getTrackAngle() {
        return TrackAngle.get();
    }

    public SimpleIntegerProperty trackAngleProperty() {
        return TrackAngle;
    }

    public void setTrackAngle(Integer trackAngle) {
        this.TrackAngle.set(trackAngle);
    }

    public double getLatitude() {
        return Latitude.get();
    }

    public SimpleDoubleProperty latitudeProperty() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        this.Latitude.set(latitude);
    }

    public double getLongitude() {
        return Longitude.get();
    }

    public SimpleDoubleProperty longitudeProperty() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        this.Longitude.set(longitude);
    }

    public double getVerticalRate() {
        return VerticalRate.get();
    }

    public SimpleDoubleProperty verticalRateProperty() {
        return VerticalRate;
    }

    public void setVerticalRate(Double verticalRate) {
        this.VerticalRate.set(verticalRate);
    }

    public int getSquawk() {
        return Squawk.get();
    }

    public SimpleIntegerProperty squawkProperty() {
        return Squawk;
    }

    public void setSquawk(Integer squawk) {
        this.Squawk.set(squawk);
    }

    public double getDistance() {
        return Distance.get();
    }

    public SimpleDoubleProperty distanceProperty() {
        return Distance;
    }

    public void setDistance(double distance) {
        this.Distance.set(distance);
    }

    public Byte getAlert() {
        return Alert;
    }

    public void setAlert(Byte alert) {
        Alert = alert;
    }

    public Byte getEmergency() {
        return Emergency;
    }

    public void setEmergency(Byte emergency) {
        Emergency = emergency;
    }

    public Byte getSPI() {
        return SPI;
    }

    public void setSPI(Byte SPI) {
        this.SPI = SPI;
    }

    public Byte getIsOnEarth() {
        return IsOnEarth;
    }

    public void setIsOnEarth(Byte isOnEarth) {
        IsOnEarth = isOnEarth;
    }

    public String getLastSignal() {
        return LastSignal.get();
    }

    public SimpleStringProperty lastSignalProperty() {
        return LastSignal;
    }

    public void setLastSignal(String lastSignal) {
        this.LastSignal.set(lastSignal);
    }

    public Date getCurrentDateTime() {
        return CurrentDateTime;
    }

    public void setCurrentDateTime(Date currentDateTime) {
        CurrentDateTime = currentDateTime;
    }
}
