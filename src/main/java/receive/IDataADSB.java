package receive;

public interface IDataADSB {
    void OnConnected(Boolean isConnected);
    void OnPropertiesData(ADSBModel adsbModel);
}
