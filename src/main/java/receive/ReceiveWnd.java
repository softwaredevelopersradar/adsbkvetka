package receive;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class ReceiveWnd extends AnchorPane implements Initializable {

    public ReceiveWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ReceiveWnd.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<IDataADSB> listeners = new ArrayList<>();

    public void addListenerDataADSB(IDataADSB toAdd) {
        listeners.add(toAdd);
    }

    Socket socket;
    Date date = new Date();
    int counter;

    List<String> listOfData = new ArrayList<>();

    String[] substr;

    ADSBModel ads_b_model = new ADSBModel();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
//        try {
//            socket = new Socket("169.254.0.1", 30003);
//        } catch (Exception ignored) {
//        }
    }

    public void connectADSB() throws IOException {
        SocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName("169.254.0.1"), 30003);

        try {
            socket = new Socket("169.254.0.1", 30003);
        } catch (Exception ignored) {
        }

        if (socket == null) {
            for (IDataADSB iDataADSB : listeners)
                iDataADSB.OnConnected(false);
        } else if (socket.isClosed() || socket.isInputShutdown() || socket.isOutputShutdown()) {
            socket.connect(socketAddress);
            socket.setKeepAlive(true);
            for (IDataADSB iDataADSB : listeners)
                iDataADSB.OnConnected(true);
        } else {
            for (IDataADSB iDataADSB : listeners)
                iDataADSB.OnConnected(true);
        }
    }

    public void disconnectADSB() throws IOException {
        if (socket != null) {
            socket.shutdownInput();
            socket.shutdownOutput();
            socket.close();
            for (IDataADSB iDataADSB : listeners)
                iDataADSB.OnConnected(false);
        }
    }

    public void getData() throws IOException {

        Runnable task = () -> {
            try {
                /**Запись из метода**/
//                SettingStringList();
//
//                while (listOfData.size() > counter) {
//
//                    String str = listOfData.get(counter);

                    /**Запись из буфера при подключённом сокете**/
                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while (input.readLine() != null) {

                    String str = input.readLine();
                    /***********************************************/

                    substr = str.split(",");

                    SettingFields();

                    ads_b_model.setLastSignal("0 sec");

                    long currentTime = date.getTime();
                    long receivedTime = ads_b_model.getGeneratedTime().getTime() + ads_b_model.getGeneratedDate().getTime();
                    long lastSignalLong = (currentTime - receivedTime) / 1000;
//                    String hmsUTC = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(lastSignalLong),
//                            TimeUnit.MILLISECONDS.toMinutes(lastSignalLong) % TimeUnit.HOURS.toMinutes(1),
//                            TimeUnit.MILLISECONDS.toSeconds(lastSignalLong) % TimeUnit.MINUTES.toSeconds(1));
                    if (lastSignalLong < 1000)
                        ads_b_model.setLastSignal(lastSignalLong + " sec");
                    else ads_b_model.setLastSignal("∞ sec");

                    ads_b_model.setCurrentDateTime(new Date());

                    for (IDataADSB iDataADSB : listeners)
                        iDataADSB.OnPropertiesData(ads_b_model);

//                }
                    counter++;
//                scanner.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        };
        new Thread(task).start();

//        }
    }

    // region TEST Generated ADSB model
    private String GeneratedStringTEST() {

        Random random = new Random();
//        String symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String symbols = "AA2";
        String strICAO = new Random().ints(3, 0, symbols.length())
                .mapToObj(symbols::charAt)
                .map(Object::toString)
                .collect(Collectors.joining());

        String strLat = String.valueOf(ThreadLocalRandom.current().nextDouble(53.0D, 60.0D));
        String strLon = String.valueOf(ThreadLocalRandom.current().nextDouble(25.0D, 33.0D));
        String strAlt = String.valueOf(ThreadLocalRandom.current().nextInt(100, 10000));


//        strICAO = "AYE8D9";
//        strLat = "500.0";
//        strLon = "500.0";

        return "MSG,3,111,11111," + strICAO + ",111111,2022/05/25,12:24:21.906,2022/05/25,12:24:21.906,," + strAlt + ",246,450," + strLat + "," + strLon + ",56.888,557,0,0,0,0";
    }

    private ADSBModel StringParserTEST() throws ParseException  {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm:ss");
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));

        for (int i = 0; i < substr.length; i++) {
            switch (i) {

                case 0 -> ads_b_model.setMessageType(substr[0]);
                case 1 -> ads_b_model.setTransmissionType(Byte.parseByte(substr[1]));
                case 2 -> ads_b_model.setSessionId(substr[2]);
                case 3 -> ads_b_model.setAircraftId(substr[3]);
                case 4 -> ads_b_model.setICAO(substr[4]);
                case 5 -> ads_b_model.setFlightId(substr[5]);
                case 6 -> ads_b_model.setGeneratedDate(format1.parse(substr[6]));
                case 7 -> ads_b_model.setGeneratedTime(format2.parse(substr[7]));
                case 8 -> ads_b_model.setLoggedDate(format1.parse(substr[8]));
                case 9 -> ads_b_model.setLoggedTime(format2.parse(substr[9]));
                case 10 -> ads_b_model.setCallsign(substr[10]);
                case 11 -> ads_b_model.setAltitude(Integer.parseInt(substr[11]));
                case 12 -> ads_b_model.setGroundSpeed(Integer.parseInt(substr[12]));
                case 13 -> ads_b_model.setTrackAngle(Integer.parseInt(substr[13]));
                case 14 -> ads_b_model.setLatitude(BigDecimal.valueOf(Double.parseDouble(substr[14])).setScale(4, RoundingMode.HALF_UP).doubleValue());
                case 15 -> ads_b_model.setLongitude(BigDecimal.valueOf(Double.parseDouble(substr[15])).setScale(4, RoundingMode.HALF_UP).doubleValue());
                case 16 -> ads_b_model.setVerticalRate(Double.parseDouble(substr[16]));
                case 17 -> ads_b_model.setSquawk(Integer.parseInt(substr[17]));
                case 18 -> ads_b_model.setAlert(Byte.parseByte(substr[18]));
                case 19 -> ads_b_model.setEmergency(Byte.parseByte(substr[19]));
                case 20 -> ads_b_model.setSPI(Byte.parseByte(substr[20]));
                case 21 -> ads_b_model.setIsOnEarth(Byte.parseByte(substr[21]));
            }
        }
        ads_b_model.setCurrentDateTime(new Date());
        return ads_b_model;
    }

    public void getDataTEST() {

        Runnable task = () -> {
            try {

                while (true) {

                    Thread.sleep(1000);
                    substr = GeneratedStringTEST().split(",");

                    for (IDataADSB iDataADSB : listeners)
                        iDataADSB.OnPropertiesData(StringParserTEST());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        };
        new Thread(task).start();
    }
    // endregion Generated


    public void SettingStringList() {

        listOfData.add("MSG,4,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,5,111,11111,4C3C63,111111,2022/05/25,12:24:21.826,2022/05/25,12:24:21.826,,39975,,,,,,,0,,0,0");
        listOfData.add("MSG,3,111,11111,4C3C63,111111,2022/05/25,12:24:21.906,2022/05/25,12:24:21.906,,39975,,,54.29970,27.77285,,,0,0,0,0");
        listOfData.add("MSG,3,111,11111,4C3C63,111111,2022/05/25,12:24:21.906,2022/05/25,12:24:21.906,,39975,,,54.79970,28.77285,,,0,0,0,0");
        listOfData.add("MSG,4,111,11111,4C3D63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,AFL8514,,450,150,,,0,,,,,");
        listOfData.add("MSG,1,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,2,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,3,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,4,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,5,111,11111,4C3F63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,100,53.29970,26.77285,0,,,,,");
        listOfData.add("MSG,6,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,7,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,8,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,4,111,11111,3C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,145,68.29970,22.77285,0,,,,,");
        listOfData.add("MSG,4,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,246,,,0,,,,,");
        listOfData.add("MSG,4,111,11111,4C3C63,111111,2022/05/25,12:24:21.666,2022/05/25,12:24:21.666,,,450,0,,,0,,,,,");
    }

    public void SettingFields() throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm:ss.SSS");
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i = 0; i < substr.length; i++) {
            if (!substr[i].equals("")) {
                switch (i) {
                    case 0 -> ads_b_model.setMessageType(substr[0]);
                    case 1 -> ads_b_model.setTransmissionType(Byte.parseByte(substr[1]));
                    case 2 -> ads_b_model.setSessionId(substr[2]);
                    case 3 -> ads_b_model.setAircraftId(substr[3]);
                    case 4 -> ads_b_model.setICAO(substr[4]);
                    case 5 -> ads_b_model.setFlightId(substr[5]);
                    case 6 -> ads_b_model.setGeneratedDate(format1.parse(substr[6]));
                    case 7 -> ads_b_model.setGeneratedTime(format2.parse(substr[7]));
                    case 8 -> ads_b_model.setLoggedDate(format1.parse(substr[8]));
                    case 9 -> ads_b_model.setLoggedTime(format2.parse(substr[9]));
                    case 10 -> ads_b_model.setCallsign(substr[10]);
                    case 11 -> ads_b_model.setAltitude(Integer.parseInt(substr[11]));
                    case 12 -> ads_b_model.setGroundSpeed(Integer.parseInt(substr[12]));
                    case 13 -> ads_b_model.setTrackAngle(Integer.parseInt(substr[13]));
                    case 14 -> ads_b_model.setLatitude(BigDecimal.valueOf(Double.parseDouble(substr[14])).setScale(4, RoundingMode.HALF_UP).doubleValue());
                    case 15 -> ads_b_model.setLongitude(BigDecimal.valueOf(Double.parseDouble(substr[15])).setScale(4, RoundingMode.HALF_UP).doubleValue());
                    case 16 -> ads_b_model.setVerticalRate(Double.parseDouble(substr[16]));
                    case 17 -> ads_b_model.setSquawk(Integer.parseInt(substr[17]));
                    case 18 -> ads_b_model.setAlert(Byte.parseByte(substr[18]));
                    case 19 -> ads_b_model.setEmergency(Byte.parseByte(substr[19]));
                    case 20 -> ads_b_model.setSPI(Byte.parseByte(substr[20]));
                    case 21 -> ads_b_model.setIsOnEarth(Byte.parseByte(substr[21]));
                }
            } else if (substr[i].equals("")) {
                switch (i) {
                    case 0 -> ads_b_model.setMessageType(null);
                    case 1 -> ads_b_model.setTransmissionType(null);
                    case 2 -> ads_b_model.setSessionId(null);
                    case 3 -> ads_b_model.setAircraftId(null);
                    case 4 -> ads_b_model.setICAO(null);
                    case 5 -> ads_b_model.setFlightId(null);
                    case 6 -> ads_b_model.setGeneratedDate(null);
                    case 7 -> ads_b_model.setGeneratedTime(null);
                    case 8 -> ads_b_model.setLoggedDate(null);
                    case 9 -> ads_b_model.setLoggedTime(null);
                    case 10 -> ads_b_model.setCallsign(null);
                    case 11 -> ads_b_model.setAltitude(-1);
                    case 12 -> ads_b_model.setGroundSpeed(-1);
                    case 13 -> ads_b_model.setTrackAngle(-1);
                    case 14 -> ads_b_model.setLatitude(500.0);
                    case 15 -> ads_b_model.setLongitude(500.0);
                    case 16 -> ads_b_model.setVerticalRate(-1.0);
                    case 17 -> ads_b_model.setSquawk(-1);
                    case 18 -> ads_b_model.setAlert(null);
                    case 19 -> ads_b_model.setEmergency(null);
                    case 20 -> ads_b_model.setSPI(null);
                    case 21 -> ads_b_model.setIsOnEarth(null);
                }

            }
        }

    }


}
